
var app = angular.module('AngularApp',
    // register angular bootstrap directives
    ['ui.bootstrap']);

// Main Angular controller for app.
function AppController($scope, $http) {

    // code for alerts
    $scope.alerts = [];

    // close option for alerts
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    }

    $scope.dismissAllAlerts = function(){
        $scope.alerts = [];
    }

    // the apps configuration last loaded
    var config = {};
    var configResolved = false;

    // tabs have to be on scope so we can use them to build UI
    $scope.tabs = {};

    // selected file
    var chosenEntry = null;
    // show contents of file for verification before setting
    $scope.text = null;

    // a title for the app
    $scope.apptitle = "Remote Chrome App";

    // on init of app always see if we can reload the config here
    chrome.storage.local.get('config', function(data){
        if( data.config ){
            console.log('preloading previously stored configuration: '+data.config);
            // and load this config
            $scope.initConfig(data.config);
        }
    })

    // initialise all pins according to config after loading
    $scope.initConfig = function(optionalConfig) {

        //$scope.alerts.push({type: 'info', msg: 'Initialising system...'});

        // load the file with $http instead here, no more need for require
        //$http.get('../config/pinout.json').then(function(e) {
        if( !optionalConfig && $scope.text) {
            config = angular.fromJson($scope.text);
            console.log("loading config from loaded text file");
        } else if (optionalConfig ) {
            if (typeof optionalConfig === 'string') {
                config = angular.fromJson(optionalConfig);
            } else
                config = optionalConfig;
            console.log("loading config from preloaded config at local storage");
        } else {
            console.log("no configuration to load...");
            return;
        }

        // store this for later use
        chrome.storage.local.set({'config': config});

        //set the basic auth
        if( config.basicauth )
            $http.defaults.headers.common.Authorization = 'Basic ' + config.basicauth;

        // clear json
        delete $scope.text;

        //  config = e.data;
        configResolved = true;
        $scope.tabs = config.tabs;

        // set all pins in output mode based on config
        for( tab in config.tabs )
            for( row in config.tabs[tab].rows )
                for ( pin in config.tabs[tab].rows[row].pins ){
                    console.log("pin iteration: "+pin);

                    //HTTP POST /GPIO/(gpioNumber)/function/("in" or "out" or "pwm")
                    $http({method: 'POST', url: config.piaddress+'/GPIO/'+config.tabs[tab].rows[row].pins[pin].number+'/function/out'}).
                        success(function(data, status, headers, config) {
                            // this callback will be called asynchronously
                            // when the response is available
                        }).
                        error(function(data, status, headers, config) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                            $scope.alerts.push({type: 'success', msg: 'config failed for pin!'});
                        });
                }

        // update app title
        if( config.apptitle )
            $scope.apptitle = config.apptitle;

        // FIXME this is out of sync!!!
        //    $scope.alerts.pop();
        //    $scope.alerts.push({type: 'success', msg: 'Initialisation done'});

        //});

        // hide the form
        $scope.paste = false;

    }

    // this toggles a single pin
    $scope.togglePin = function(number) {

        // we need the configuration to be in place so check that first
        if(!configResolved)
            $scope.initConfig();
        // TODO work with promise here ...

        // output bit sequence
        // HTTP POST /GPIO/(gpioNumber)/sequence/(delay),(sequence)
        $http({method: 'POST', url: config.piaddress+'/GPIO/'+number+'/sequence/200,010'}).
            success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.alerts.push({type: 'danger', msg: 'Pin toggle failed!'});
            });

    }

    // gets a file to be set for configuration
    $scope.pickFile = function (){
        var accepts = [{
            mimeTypes: ['text/*'],
            // might want to limit types here
            extensions: ['json', 'js', 'css', 'txt', 'html', 'xml', 'tsv', 'csv', 'rtf']
        }];
        chrome.fileSystem.chooseEntry({type: 'openFile', accepts: accepts}, function(theEntry) {
            if (!theEntry) {
                $scope.alerts.push({type: 'info', msg: 'No file selected.'});
                // since this happens after loading file ze need to trigger digest cycle
                $scope.$digest();
                return;
            }
            // use local storage to retain access to this file
            chrome.storage.local.set({'chosenFile': chrome.fileSystem.retainEntry(theEntry)});
            loadFileEntry(theEntry);

        });
    }

    // remove previously selected file
    $scope.resetUpload = function(){
        $scope.text = null;
    }

    // somehow this has to be in place instead of the in view expression in order for the toggle to work...
    $scope.togglePaste = function(){
        $scope.paste = !$scope.paste;
    }

    // for files, read the text content
    function loadFileEntry(_chosenEntry) {
        chosenEntry = _chosenEntry;
        chosenEntry.file(function(file) {
            readAsText(chosenEntry, function(result) {
                $scope.text = result;
                // since this happens after loading file ze need to trigger digest cycle
                $scope.$digest();
            });
        });
    }

    function readAsText(fileEntry, callback) {
        fileEntry.file(function(file) {
            var reader = new FileReader();

            reader.onerror = errorHandler;
            reader.onload = function(e) {
                callback(e.target.result);
            };

            reader.readAsText(file);
        });
    }

    function errorHandler(e) {
        console.error(e);
    }
}

AppController.$inject = ['$scope', '$http']; // For code minifiers.