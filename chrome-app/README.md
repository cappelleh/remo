# About

This is the Chrome app. This is an Angular app that can be imported in chrome as an app or built for mobile devices running iOS or Android. 

# Getting Started

This directory only contains the Chrome App resources. Use the Chrome App for Mobile toolchain to create a mobile project from these resources. 

## Prepare Dev Env

* Install Android Development Tools and/or OS X Development Tools
* Install Chrome Apps for mobile toolchain (cca)

Execute the following commands to get the toolchain installed (if you have npm installed already):

    npm install -g cca
    cca checkenv

More information about these steps are available at [Chrome Installation instructions](https://github.com/MobileChromeApps/mobile-chrome-apps/blob/master/docs/Installation.md)

## Build using Grunt

Grunt has to be installed, use npm to do so.

    npm install -g grunt-cli

Start by installing dependencies using npm from within the projects directory execute:

    npm install

Next use grunt to compile the jade files:

    grunt

This will generate the views/main.html file and the js/app.js and js/background.js files.

## Building for Chrome

You can either build this project for chrome. For this use the [Your first Chrome app instructions](https://developer.chrome.com/apps/first_app) as a guideline.
[Step 5: Launch your App](https://developer.chrome.com/apps/first_app#five) should have all the required details.

## Building for Mobile

Create a project with the cca toolchain using the existing manifest.json in this folder. When you do so a manifest.mobile.json config file will be created.

    cca create YourApp --link-to=path/to/manifest.json

# Debug

## Android & Chrome App

Use chrome, connect android device over USB and navigate to [chrome://inspect](chrome://inspect/)

## iOS

TODO


