module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jade: {
            compile: {
                options: {
                    data: {
                        debug: false
                    }
                },
                files: [ {
                    expand: true,
                    cwd: 'src/views',
                    src: '**/*.jade',
                    dest: 'views',
                    ext: '.html'
                } ]
            }
        },
        uglify: {
            dev: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    beautify: true,
                    mangle: false,
                    compress: false
                },
                files: [{
                    expand: true,
                    cwd: 'src/scripts',
                    src: '**/*.js',
                    dest: 'js/scripts'
                }]
            },
            staging: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                },
                files: [{
                    expand: true,
                    cwd: 'src/scripts',
                    src: '**/*.js',
                    dest: 'js/scripts'
                }]
            }
        },
        watch: {
            templates:{
                files: ['src/views/*.jade'],
                tasks: ['jade']
            },
            scripts: {
                files: ['src/scripts/*.js'],
                tasks: ['uglify:dev']
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-watch');

    //Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    //Default task(s).
    grunt.registerTask('default', ['jade', 'uglify:staging']);
    grunt.registerTask('dev', ['jade', 'uglify:dev']);

};
