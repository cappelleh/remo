/**
 * Remote controlled power plugs by hcpl. 
 * More information at redmine.hcpl.be
 */

// LIBRARIES
// Android meets arduino lib for BT communication with Android
// http://amarino-toolkit.net for more information
#include <MeetAndroid.h>

// CONSTANTS
#define PIN_1_ON 6
#define PIN_1_OFF 9
// our BT communication has to be set tot he right baud rate
#define BAUDRATE 9600 
#define DELAY_TIME 250

// Android meets Arduino
MeetAndroid meetAndroid;

// init
void setup(){
    // use the baud rate your bluetooth module is configured to 
  // not all baud rates are working well, i.e. ATMEGA168 works best with 57600
  Serial.begin(BAUDRATE); 
  // assign pins
  pinMode(PIN_1_ON, OUTPUT); 
  pinMode(PIN_1_OFF, OUTPUT); 
  
  // initialise all off
  digitalWrite(PIN_1_ON,LOW);  
  digitalWrite(PIN_1_OFF,LOW);

  // for debug information
  //Serial.begin(9600);

  // register callback functions, which will be called when an associated event occurs.
  // - the first parameter is the name of your function (see below)
  // - match the second parameter ('A', 'B', 'a', etc...) with the flag on your Android application
  meetAndroid.registerFunction(toggleON, 'O');
  meetAndroid.registerFunction(toggleOFF, 'F');
}

// main loop
void loop(){
  // input
  //int in = Serial.read();
  //Serial.println(in);
  meetAndroid.receive();
  
  //trigger ON
  //Serial.println("ON");
  //digitalWrite(PIN_1_ON, HIGH);
  //delay(250);
  //digitalWrite(PIN_1_ON, LOW);
  
  //delay(random(3,6)*1000);
  
  // trigger OFF
  //Serial.println("OFF");
  //digitalWrite(PIN_1_OFF, HIGH);
  //delay(250);
  //digitalWrite(PIN_1_OFF, LOW);
  
  //delay(random(3,6)*1000);
  
}

void toggleON(byte flag, byte count){
  digitalWrite(PIN_1_ON, HIGH);
  delay(DELAY_TIME);
  digitalWrite(PIN_1_ON, LOW);
  
}

void toggleOFF(byte flag, byte count){
  digitalWrite(PIN_1_OFF, HIGH);
  delay(DELAY_TIME);
  digitalWrite(PIN_1_OFF, LOW);
}
