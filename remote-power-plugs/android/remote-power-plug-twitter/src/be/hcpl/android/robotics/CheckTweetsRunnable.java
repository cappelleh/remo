package be.hcpl.android.robotics;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import at.abraxas.amarino.Amarino;

/**
 * runnable for checking for tweets using the twitter search api
 * 
 * @author hcpl
 * 
 */
public class CheckTweetsRunnable implements Runnable {

	private Set<Long> handledIDs = new HashSet<Long>();

	private boolean alive = true;

	private Context context = null;

	private String address = null;

	private TextView textView = null;

	public CheckTweetsRunnable(Context context, String address,
			TextView textView) {
		super();
		this.context = context;
		this.address = address;
		this.textView = textView;
	}

	@Override
	public void run() {
		// keep running
		while (alive) {
			// we need a http client to perform the http request
			HttpClient client = new DefaultHttpClient();
			// for now search only single one at a time, this returns json
			HttpGet get = new HttpGet(
					"http://search.twitter.com/search.json?q=@hcpl_test&rpp=1&page=1");
			try {
				// handler to get the content
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				String content = client.execute(get, responseHandler);
				// now parse the json object
				JSONObject json = new JSONObject(content);
				// get message details
				JSONArray results = json.getJSONArray("results");
				JSONObject result = results.getJSONObject(0);
				long id = result.getLong("id");
				String text = result.getString("text");
				// check if id exists
				if (!handledIDs.contains(id)) {
					// handle
					handleText(text);
					// update handledIDs
					handledIDs.add(id);
				} else {
					Log.i(this.getClass().getName(),
							"Ignored duplicate message");
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				Log.e(this.getClass().getName(), e.getMessage(), e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				Log.e(this.getClass().getName(), e.getMessage(), e);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				Log.e(this.getClass().getName(), e.getMessage(), e);
			}
			//wait for some time to let other internet activities happen
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private Handler handler = new Handler();

	private void handleText(final String text) {
		if (text == null)
			return;
		Log.i(this.getClass().getName(), "handle message: " + text);
		// post the text to gui first
		handler.post(new Runnable() {
			public void run() {
				textView.setText(text + "\r\n" + textView.getText());
			}
		});
		if (text.indexOf("ON") > -1) {
			Amarino.sendDataToArduino(context, address, 'O', 0);
			Log.i(this.getClass().getName(), "ON command send to device");
		} else if (text.indexOf("OFF") > -1) {
			Amarino.sendDataToArduino(context, address, 'F', 0);
			Log.i(this.getClass().getName(), "OFF command send to device");
		}

	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

}
