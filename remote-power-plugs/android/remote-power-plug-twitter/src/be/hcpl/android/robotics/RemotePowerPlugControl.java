/*
	More info at http://redmine.hcpl.be
 */
package be.hcpl.android.robotics;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;
import at.abraxas.amarino.Amarino;

/**
 * main activity for twitter enabled control app
 * 
 * @author hcpl
 * 
 */
public class RemotePowerPlugControl extends Activity {

	private static final String TAG = "RemotePowerPlugControl";

	/*
	 * TODO: change the address to the address of your Bluetooth module and
	 * ensure your device is added to Amarino
	 */
	private static final String DEVICE_ADDRESS = "00:11:09:25:04:57";

	private CheckTweetsRunnable runnable;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Amarino.connect(this, DEVICE_ADDRESS);

		// start listening to twitter also
		// FIXME don't do it this way, only a quick solution!!
		runnable = new CheckTweetsRunnable(RemotePowerPlugControl.this,
				DEVICE_ADDRESS, (TextView) findViewById(R.id.twitterContent));
		// handler.post(runnable);
		new Thread(runnable).start();
	}

	public void onToggleClicked(View view) {
		if (((ToggleButton) view).isChecked()) {
			Amarino.sendDataToArduino(RemotePowerPlugControl.this,
					DEVICE_ADDRESS, 'O', 0);
			Log.i(TAG, "ON command send to device");
		} else {
			Amarino.sendDataToArduino(RemotePowerPlugControl.this,
					DEVICE_ADDRESS, 'F', 0);
			Log.i(TAG, "OFF command send to device");
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		runnable.setAlive(false);
		// stop Amarino's background service, we don't need it any more
		Amarino.disconnect(this, DEVICE_ADDRESS);
	}

}