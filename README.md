# About

An open source home automation project for DIY addicts.

This page contains the getting started guide. For more detail visit this projects wiki pages or check the README files in the source folders.

#### Help wanted! If you know someone who can print PCB's please let me know.

# Getting Started

What this readme is all about.

## Hardware

The required hardware for this project. See [project wiki](https://bitbucket.org/cappelleh/remo/wiki/Home) for all details on getting the hardware set up properly.

### Raspberry Pi preparation

You'll need at least 1 [Raspberry Pi](http://be.farnell.com/raspberry-pi/) giving you 20 controllable pins. If you need more pins you can get more raspberries (*a minor tweak to code is needed for separate baseurl configuration*).  

Prepare each Pi with an [official raspbian](http://www.raspberrypi.org/downloads) (*debian version for arm architecture of pi*) image according to [these instructions](http://www.raspberrypi.org/quick-start-guide). 

Verify the disk you need to use with the output form the below command. This will generate a list of /dev/diskXsY references that you need in the next commands. Note that this is OS X only. On linux use df -h (*if you run linux you probably don't need this information anyway*).
	
	diskutil list

Use that reference to unmount and then dd to copy the image (unzipped) to that card (here with disk1s1). Note the use of rdisk in the last command.

	diskutil unmountDisk /dev/disk1s1
	sudo dd bs=1m if=2012-12-16-wheezy-raspbian.img of=/dev/rdisk1

Connect the pi to your network or directly to your computer, boot it with this card mounted and log in over ssh to perform the installation of [webiopi](https://code.google.com/p/webiopi/).

See downloads page to get latest package, and adapt x.y.z with the version you download.

	tar xvzf WebIOPi-x.y.z.tar.gz
	cd WebIOPi-x.y.z
	sudo ./setup.sh

By default webiopi service is secured with basic auth. For this to work you need to update your in app configuration with a base64 encoded "username:password" value. The default username and password are in the example configuration file.

### Optocouplers & other connections

You'll also need to create a custom board to map each raspberry pi pin to an optocoupler to control something. 

#### Required tools & parts:

* soldering iron, skills and solder
* wire stripper and some wire (I used single core 0.8mm wire, any copper will do)
* a [prototype pcb board](http://www.goodluckbuy.com/6cm-x-8cm-double-sided-solderable-prototype-pcb-board-breadboard-5pcs.html)
* some [4N35 optocouplers](http://be.farnell.com/vishay/4n35/optocoupler-transistor-o-p-dip/dp/1328375)
* the same amount + 1 [screw terminals](http://www.goodluckbuy.com/2-pin-screw-terminal-block-connector-5mm-pitch-100-pcs.html)
* double row [female pin headers](http://www.goodluckbuy.com/40pin-2-54mm-single-row-female-header-strip-10-pack.html)

If you want to use the P5 header of your pi you need some [male headers](http://www.goodluckbuy.com/40pin-2-54mm-double-row-male-header-strip-10-pack.html) too.

#### Putting it all together

Route each raspberry pi GPIO pin (the ones with the number, GPIO0 to GPIO20) to the the anode pin of an optocoupler. Create a common ground pin for all cathodes to go to and connect that with one of the ground pins of the pi. **All this is 3V, don't mix with the 24V signals of the teleruptors!** 

On the 24V side route each teleruptor to the emitter pin of the optocoupler and a common 24V signal to all collector pins of all optocouplers.

You can power your pi with 5V, around 1A is needed for proper functioning. Either through the 5V pin or the micro USB port. If the pi doesn't boot check power and sd card.

** I'd love to make a PCB here but I really lack the knowledge. If anyone can help please contact me!!**

## App

For chrome/mobile app check README in chrome-app folder in source for more information on setting up environment and building. I'll do my best to list the needed steps here.

At the moment no official releases (builds, packages, ...) are available so you'll have to follow these steps to get the app running.

### Chrome App

This is the easiest option. Allthough the src files (files in src directory) are handled by grunt (for minification, jade compilation, ...) I checked in the results on here. This way you should be able to get the app running with minimal effort. 

Always start by getting the latest sources. If you haven't got any yet [install git](http://git-scm.com/book/en/Getting-Started-Installing-Git) and navigate to some workspace directory to get the sources with the following command:

	git clone https://bitbucket.org/cappelleh/remo.git
	cd remo

Later on you can update your sources from this directory with:

	git pull

Now open chrome and follow [these steps](https://developer.chrome.com/apps/first_app#five) for the installation of this app from this directory. You should be able to run the app now. 

#### The steps

* Go to chrome://flags.
* Find "Experimental Extension APIs", and click its "Enable" link.
* Restart Chrome.
* Click the settings icon  and choosing "Tools" > "Extensions".
* Make sure the "Developer mode" checkbox has been selected.
* Click the "Load unpacked extension" button, navigate to your app's folder and click "OK".
* Once you've loaded your app, open a New Tab page and click on your new app icon.

### Configuration

The configuration will be empty at first run. Go to the settings tab in the app and hit the "configuration" button.
Next navigate to this same remo folder, open the config folder and select the pinout.json file. This is just an example file.
This configuration is very unlikely to match your situation but it should get you started.

The structure of the configuration is json. There is a list of tabs with the pins and some layout information per
tab/pin. To keep things simple the floorplan that can be created with this config is completely build form div elements.
Each row of div elements is wrapped around a div element with class row. The div elements themselves can be styled
using the style property in the config file. This is plain css.

*Tip: the class property is still set though so you can still use the bootstrap option. Or you could use some convenient
bootstrap classes like btn-primary to have some visual feedback*

The properties on the end of the example file are general configuration properties that match the entries you'll find on
the fixed configuration tab in the app. This tab itself is not part of this configuratino file.

 TODO create an option to hide the config tab (that way you won't be able to change settings unless.... )

[Check this example config](https://bitbucket.org/cappelleh/remo/raw/721b1f346c9a45e16e530e3f4b80dd8d13c66f23/chrome-app/config/pinout.json)


### Android

Having [npm installed](http://nodejs.org/) does make this step a lot easier. Follow these commands to install the cca toolchain:

	npm install -g cca
	cca checkenv

That last command will check if adb (and some other required tools) is installed. If not go ahead and fix that first. [Instructions here](https://github.com/MobileChromeApps/mobile-chrome-apps/blob/master/docs/Installation.md)

From the remo folder execute the following to create the mobile app project from resources:

	cca create MobileApp --link-to=chrome-app/manifest.json

Now navigate into that folder, connect your Android device over adb and start the app wth the following commands:

	cd MobileApp
	cca run android

Or you can emulate the app with:

	cca emulate android

### iOS

Not tested yet but should be very similar to the android setup thanks to the cordova approach. Just use the following command to run instead:

	cca run ios

or:

	cca emulate ios

# More Documentation

See project website and this projects wiki pages or check the README files in the source folders.

For now website is hosted at [http://hcpl.herokuapp.com/c/remo](http://hcpl.herokuapp.com/c/remo)

# License

[Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.html)
