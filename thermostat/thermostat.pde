// v1.1 07/12/2011
// ===============
//  - added force RELAY OFF
//  - fixed issue continuous on/off switching ones MAX temp is reached.
//    =>  new approach is to heat up once min is reached until max is reached
//        then stop heating until min is reached again. 
//  - optional: switch to new temp sensor ONLY WHEN A DS18B20 CHIP IS FOUND THE
//        MEASURED TEMP FROM THERMISTOR WILL BE OVERRULED

//TODO
//  - fix casing of members
//  - use relay 2
//  - use values of both temp sensors (thermistor & ds)

// which analog pin to connect the thermistor to 
#define THERMISTORPIN A0         
// resistance at 25 degrees C (check thermistor datasheet)
#define THERMISTORNOMINAL 10000      
// temp. for nominal resistance (almost always 25 C, datasheet)
#define TEMPERATURENOMINAL 25   
// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 5
// The beta coefficient of the thermistor (usually 3000-4000, datasheet)
#define BCOEFFICIENT 3977
// the value of the 'other' resistor. This other resistor is your choice,
// you can read it with mutlimeter to make sure the value is correct!
#define SERIESRESISTOR 1000  

// state led pins
#define PIN_LED_GREEN 8
#define PIN_LED_ORANGE 10
#define PIN_LED_RED 12

// relay pins
#define PIN_RELAY_1 6
#define PIN_RELAY_2 4

// min and max temp defaults
#define DEFAULT_TEMP_MIN 3
#define DEFAULT_TEMP_MAX 5

// default polling interval
#define DEFAULT_INTERVAL 5000

// amarino include, check: http://www.amarino-toolkit.net/
#include <MeetAndroid.h>

//check: http://www.amarino-toolkit.net/
MeetAndroid meetAndroid;

// perform multiple reads and get average for 
int samples[NUMSAMPLES]; //smoothing

// if relay has to be forced ON or not
boolean RELAY_1_ON = false;
boolean RELAY_1_OFF = false;

// the current interval (can be different than default if updated by BT)
int INTERVAL = DEFAULT_INTERVAL;

// current min & max temp (can be different than default if updated by BT)
int TEMP_MIN = DEFAULT_TEMP_MIN;
int TEMP_MAX = DEFAULT_TEMP_MAX;

// this is used to indicate if the circuit is in heating mode or not.
// By keeping track of this state we can heat up once MIN value is reached
// continue heating and stop at MAX temp. Then to avoid switching wait
// heating up again until MIN temp is reached.
boolean isHeating = false;

// includes for dallas temp reading
#include <OneWire.h>
#include <DallasTemperature.h>

// UPDATED TEMP SENSOR CODE
// ************************
// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2
#define TEMPERATURE_PRECISION 9

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

int numberOfDevices; // Number of temperature devices found

DeviceAddress tempDeviceAddress; // We'll use this variable to store a found device address

//
void setup(void) {
  
  // register amarino listeners to handle commands
  meetAndroid.registerFunction(handleRelayCommand, 'R');
  meetAndroid.registerFunction(handleIntervalCommand, 'I');
  meetAndroid.registerFunction(handleLowTempCommand, 'L');
  meetAndroid.registerFunction(handleHighTempCommand, 'H');
  
  // my bluetooth module requires this BAUD RATE. Make sure to use
  // the rate your BT module is set to if you want to use BT. 
  Serial.begin(115200);
  
  // set reference for comparing and calculating resistance thermistor
  analogReference(EXTERNAL);
  
  // clear all pins (important for relays so they are off by default)
  digitalWrite(PIN_RELAY_1, LOW);
  digitalWrite(PIN_RELAY_2,LOW);

  // define pins realy
  pinMode(PIN_RELAY_1, OUTPUT);
  pinMode(PIN_RELAY_2, OUTPUT);
  // define pins leds
  pinMode(PIN_LED_GREEN, OUTPUT);
  pinMode(PIN_LED_ORANGE, OUTPUT);
  pinMode(PIN_LED_RED, OUTPUT);
  
  // Start up the library
  sensors.begin();  
  // Grab a count of devices on the wire
  numberOfDevices = sensors.getDeviceCount();
  // Loop through each device, print out address
  for(int i=0;i<numberOfDevices; i++) {
    // Search the wire for address
    if(sensors.getAddress(tempDeviceAddress, i)) {
	// set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
	sensors.setResolution(tempDeviceAddress, TEMPERATURE_PRECISION);
    }
  }
  // inform about nr of devices
  meetAndroid.send("Nr of DS18B20 chips found ");
  meetAndroid.send(numberOfDevices);

}
 
//
void loop(void) {
 
  // android receive events, check listeners in setup to see what code (char) 
  // is registered to what method
  meetAndroid.receive();
  
  // don't bother reading thermistor when dallas chip is available
  // TODO 
  
  //inits
  uint8_t i;
  float average;
 
  // take N samples in a row, with a slight delay
  for (i=0; i< NUMSAMPLES; i++) {
   samples[i] = analogRead(THERMISTORPIN);
   delay(10);
  }
 
  // average all the samples out
  average = 0;
  for (i=0; i< NUMSAMPLES; i++) {
     average += samples[i];
  }
  average /= NUMSAMPLES;
 
  //Serial.print("Average analog reading "); 
  meetAndroid.send("Average analog reading from thermistor ");
  //Serial.println(average);
  meetAndroid.send(average);
 
  // convert the value to resistance
  average = 1023 / average - 1;
  average = SERIESRESISTOR / average;
  //Serial.print("Thermistor resistance "); 
  meetAndroid.send("Thermistor resistance ");
  //Serial.println(average);
  meetAndroid.send(average);
 
  float steinhart;
  steinhart = average / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C
 
  //Serial.print("Temperature "); 
  meetAndroid.send("Temperature from thermistor ");
  //Serial.print(steinhart);
  meetAndroid.send(steinhart);
  //Serial.println(" *C");
  meetAndroid.send(" *C");
  
  // ALTERNATIVE TEMP FROM DALLAS CHIP
  // ********************************
  if( numberOfDevices > 0 ){
    // call sensors.requestTemperatures() to issue a global temperature 
    // request to all devices on the bus
    //Serial.print("Requesting temperatures...");
    meetAndroid.send("Requesting temperatures from DS18B20 chips.");
    sensors.requestTemperatures(); // Send the command to get temperatures
   
    float tempC = 0;
    // Loop through each device, print out temperature data
    for(int i=0;i<numberOfDevices; i++) {
      // Search the wire for address
      if(sensors.getAddress(tempDeviceAddress, i)) {
        tempC += sensors.getTempC(tempDeviceAddress);
      } 	
    }
    // from this total get an average reading
    // FOR NOW OVERWRITE THE ANALOG THERMISTOR READING
    steinhart = tempC/numberOfDevices;
    meetAndroid.send("Average temp from DS18B20 chips: ");
    meetAndroid.send(steinhart);
  }
  
  // override relay state, in this case RELAY is forced on and
  // all leds are set HIGH to indicate no temperature readings are
  // used to set relay state
  if (RELAY_1_ON ){
    // enable relay
    meetAndroid.send("Forced RELAY ON");
    digitalWrite(PIN_RELAY_1, HIGH);
    // lit all leds
    digitalWrite(PIN_LED_GREEN, HIGH); 
    digitalWrite(PIN_LED_ORANGE, HIGH); 
    digitalWrite(PIN_LED_RED, HIGH);
  }
  // force relay OFF
  else if (RELAY_1_OFF ){
    // enable relay
    meetAndroid.send("Forced RELAY OFF");
    digitalWrite(PIN_RELAY_1, LOW);
    // lit all leds
    digitalWrite(PIN_LED_GREEN, LOW); 
    digitalWrite(PIN_LED_ORANGE, LOW); 
    digitalWrite(PIN_LED_RED, LOW);
  }  
  // otherwise check temp readings and update
  // relay according to min and max temp currently set
  // if temp >= MAX all is OK and relay is off
  else if( steinhart >= TEMP_MAX  ){
    //logging
    meetAndroid.send("Temp HIGH state");
    //temp OK
    digitalWrite(PIN_LED_GREEN, HIGH); 
    digitalWrite(PIN_LED_ORANGE, LOW); 
    digitalWrite(PIN_LED_RED, LOW);
    //relay update
    digitalWrite(PIN_RELAY_1, LOW);
    // stop heating at this point
    isHeating = false;
  } 
  // if temp is between min and max already indicate
  // with orange led and activate relay if we were heating
  // to get above max temp again
  else if ( steinhart > TEMP_MIN && steinhart < TEMP_MAX ) {
    //logging
    meetAndroid.send("Temp MID state");
    //getting close to freezing
    digitalWrite(PIN_LED_GREEN, LOW); 
    digitalWrite(PIN_LED_ORANGE, HIGH); 
    digitalWrite(PIN_LED_RED, LOW);
    // now check if we were in heating state or not
    // the heating state is set at MIN temp and switched 
    // off again at MAX temp.
    if( isHeating) {
       //relay update
      digitalWrite(PIN_RELAY_1, HIGH); 
    }
    // if we were not heating at this point the MAX was already
    // reach and we are now slowly falling back to the MIN temp
    // no need to start heating up again (avoid switching)
  } 
  // when min temp is reached keep relay on and indicate
  // with red led
  else if (steinhart <= TEMP_MIN ) {
    //logging
    meetAndroid.send("Temp LOW state");
    //too close to freezing
    digitalWrite(PIN_LED_GREEN, LOW); 
    digitalWrite(PIN_LED_ORANGE, LOW); 
    digitalWrite(PIN_LED_RED, HIGH); 
    // relay update, put it on since we want to heat up
    // again from this point.
    digitalWrite(PIN_RELAY_1, HIGH);
    // By marking the isHeating state we ensure that once
    // the MID temp is reached we still heat up until the 
    // MAX temp is reached and isHeating can be unset and
    // relay switched on until we reach this MIN temp again
    isHeating = true;
  }
   // variable interval
  delay(INTERVAL);
}

/**
 * handle relay state change command
 */
void handleRelayCommand(byte flag, byte numOfValues){
  //get the passed value
  int command = meetAndroid.getInt();
  //and based on that command do something
  // force relay on
  if( command == 1){
    RELAY_1_ON = true;
    RELAY_1_OFF = false;
  } 
  // force relay off
  else if (command == 0 ) {
    RELAY_1_ON = false;
    RELAY_1_OFF = true;
  }
  // use all other input to return to default behaviour
  else {
    RELAY_1_ON = false;
    RELAY_1_OFF = false;
  } 
  // inform through serial, need to send strings and other datatypes
  // seperately because we get @#"é'!àçé otherwise
  meetAndroid.send("relay forced to ");
  meetAndroid.send(command);
}

/**
 * handle interval update command
 */
void handleIntervalCommand(byte flag, byte numOfValues){
  // get passed value
  INTERVAL = meetAndroid.getLong();
  // and inform
  meetAndroid.send("interval updated to ");
  meetAndroid.send(INTERVAL); 
}

/**
 * handle update of min temp
 */
void handleLowTempCommand(byte flag, byte numOfValues){
  TEMP_MIN = meetAndroid.getInt();
  meetAndroid.send("low temp updated to ");
  meetAndroid.send(INTERVAL);   
}

/**
 * update max temp
 */
void handleHighTempCommand(byte flag, byte numOfValues){
  TEMP_MAX = meetAndroid.getInt();
  meetAndroid.send("high temp updated to ");
  meetAndroid.send(INTERVAL); 
}  
  
