// v2.0 07/12/2012
// ===============
// - removed thermistor logic
// - removed bluetooth connection (to undo add imports again and replace serial.println with meetAndroid.send)
// - reduced nr of relays
// - converted for Arduino 1.0 compatibility, need updated libraries
// - reduced baud rate to 9600

// v1.1 07/12/2011
// ===============
//  - added force RELAY OFF
//  - fixed issue continuous on/off switching ones MAX temp is reached.
//    =>  new approach is to heat up once min is reached until max is reached
//        then stop heating until min is reached again. 
//  - optional: switch to new temp sensor ONLY WHEN A DS18B20 CHIP IS FOUND THE
//        MEASURED TEMP FROM THERMISTOR WILL BE OVERRULED

//TODO
//  - fix casing of members
//  - use relay 2
//  - use values of both temp sensors (thermistor & ds)

// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 5

// state led pins
#define PIN_LED_GREEN 8
#define PIN_LED_ORANGE 6
#define PIN_LED_RED 2

// relay pins
#define PIN_RELAY_1 10

// min and max temp defaults
#define DEFAULT_TEMP_MIN 3
#define DEFAULT_TEMP_MAX 5

// default polling interval
#define DEFAULT_INTERVAL 5000

// perform multiple reads and get average for 
int samples[NUMSAMPLES]; //smoothing

// if relay has to be forced ON or not
boolean RELAY_1_ON = false;
boolean RELAY_1_OFF = false;

// the current interval (can be different than default if updated by BT)
int INTERVAL = DEFAULT_INTERVAL;

// current min & max temp (can be different than default if updated by BT)
int TEMP_MIN = DEFAULT_TEMP_MIN;
int TEMP_MAX = DEFAULT_TEMP_MAX;

// this is used to indicate if the circuit is in heating mode or not.
// By keeping track of this state we can heat up once MIN value is reached
// continue heating and stop at MAX temp. Then to avoid switching wait
// heating up again until MIN temp is reached.
boolean isHeating = false;

// includes for dallas temp reading
#include <OneWire.h>
#include <DallasTemperature.h>

// UPDATED TEMP SENSOR CODE
// ************************
// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 12
#define TEMPERATURE_PRECISION 9

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

int numberOfDevices; // Number of temperature devices found

DeviceAddress tempDeviceAddress; // We'll use this variable to store a found device address

// global var for the temperature
float tempC=0;

// the on/off switching of the relay (depends on wiring)
const int ON = LOW;
const int OFF = HIGH;

// init
void setup(void) {
  
  // register amarino listeners to handle commands
//  meetAndroid.registerFunction(handleRelayCommand, 'R');
//  meetAndroid.registerFunction(handleIntervalCommand, 'I');
//  meetAndroid.registerFunction(handleLowTempCommand, 'L');
//  meetAndroid.registerFunction(handleHighTempCommand, 'H');
  
  // my bluetooth module requires this BAUD RATE. Make sure to use
  // the rate your BT module is set to if you want to use BT. 
  Serial.begin(9600);
  
  // set reference for comparing and calculating resistance thermistor
//  analogReference(EXTERNAL);
  
  // clear all pins (important for relays so they are off by default)
  digitalWrite(PIN_RELAY_1, OFF);
  // define pins realy
  pinMode(PIN_RELAY_1, OUTPUT);
  // clear all pins (important for relays so they are off by default)
  digitalWrite(PIN_RELAY_1, OFF);
  
  // define pins leds
  pinMode(PIN_LED_GREEN, OUTPUT);
  pinMode(PIN_LED_ORANGE, OUTPUT);
  pinMode(PIN_LED_RED, OUTPUT);
  
  // Start up the library
  sensors.begin();  
  // Grab a count of devices on the wire
  numberOfDevices = sensors.getDeviceCount();
  // Loop through each device, print out address
  for(int i=0;i<numberOfDevices; i++) {
    // Search the wire for address
    if(sensors.getAddress(tempDeviceAddress, i)) {
	// set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
	sensors.setResolution(tempDeviceAddress, TEMPERATURE_PRECISION);
    }
  }
  // inform about nr of devices
  Serial.print("Nr of DS18B20 chips found ");
  Serial.println(numberOfDevices);

}
 
//
void loop(void) {
 
  // android receive events, check listeners in setup to see what code (char) 
  // is registered to what method
  //meetAndroid.receive();
     
  // ALTERNATIVE TEMP FROM DALLAS CHIP
  // ********************************
  if( numberOfDevices > 0 ){
    // call sensors.requestTemperatures() to issue a global temperature 
    // request to all devices on the bus
    //Serial.print("Requesting temperatures...");
    Serial.println("Requesting temperatures from DS18B20 chips.");
    sensors.requestTemperatures(); // Send the command to get temperatures
   
   //FIXME add averaging again?
   tempC=0;
    // Loop through each device, print out temperature data
    for(int i=0;i<numberOfDevices; i++) {
      // Search the wire for address
      if(sensors.getAddress(tempDeviceAddress, i)) {
        tempC += sensors.getTempC(tempDeviceAddress);
      } 	
    }
    // from this total get an average reading
    // FOR NOW OVERWRITE THE ANALOG THERMISTOR READING
    tempC = tempC/numberOfDevices;
    Serial.print("Average temp from DS18B20 chips: ");
    Serial.println(tempC);
  }
  
  // override relay state, in this case RELAY is forced on and
  // all leds are set HIGH to indicate no temperature readings are
  // used to set relay state
  if (RELAY_1_ON ){
    // enable relay
    Serial.println("Forced RELAY ON");
    digitalWrite(PIN_RELAY_1, ON);
    // lit all leds
    digitalWrite(PIN_LED_GREEN, HIGH); 
    digitalWrite(PIN_LED_ORANGE, HIGH); 
    digitalWrite(PIN_LED_RED, HIGH);
  }
  // force relay OFF
  else if (RELAY_1_OFF ){
    // enable relay
    Serial.println("Forced RELAY OFF");
    digitalWrite(PIN_RELAY_1, OFF);
    // lit all leds
    digitalWrite(PIN_LED_GREEN, LOW); 
    digitalWrite(PIN_LED_ORANGE, LOW); 
    digitalWrite(PIN_LED_RED, LOW);
  }  
  // otherwise check temp readings and update
  // relay according to min and max temp currently set
  // if temp >= MAX all is OK and relay is off
  else if( tempC >= TEMP_MAX  ){
    //logging
    Serial.println("Temp HIGH state");
    //temp OK
    digitalWrite(PIN_LED_GREEN, HIGH); 
    digitalWrite(PIN_LED_ORANGE, LOW); 
    digitalWrite(PIN_LED_RED, LOW);
    //relay update
    digitalWrite(PIN_RELAY_1, OFF);
    // stop heating at this point
    isHeating = false;
  } 
  // if temp is between min and max already indicate
  // with orange led and activate relay if we were heating
  // to get above max temp again
  else if ( tempC > TEMP_MIN && tempC < TEMP_MAX ) {
    //logging
    Serial.println("Temp MID state");
    //getting close to freezing
    digitalWrite(PIN_LED_GREEN, LOW); 
    digitalWrite(PIN_LED_ORANGE, HIGH); 
    digitalWrite(PIN_LED_RED, LOW);
    // now check if we were in heating state or not
    // the heating state is set at MIN temp and switched 
    // off again at MAX temp.
    if( isHeating) {
       //relay update, let's continue heating
      digitalWrite(PIN_RELAY_1, ON); 
    }
    // if we were not heating at this point the MAX was already
    // reach and we are now slowly falling back to the MIN temp
    // no need to start heating up again (avoid switching)
  } 
  // when min temp is reached keep relay on and indicate
  // with red led
  else if (tempC <= TEMP_MIN ) {
    //logging
    Serial.println("Temp LOW state");
    //too close to freezing
    digitalWrite(PIN_LED_GREEN, LOW); 
    digitalWrite(PIN_LED_ORANGE, LOW); 
    digitalWrite(PIN_LED_RED, HIGH); 
    // relay update, put it on since we want to heat up
    // again from this point.
    digitalWrite(PIN_RELAY_1, ON);
    // By marking the isHeating state we ensure that once
    // the MID temp is reached we still heat up until the 
    // MAX temp is reached and isHeating can be unset and
    // relay switched on until we reach this MIN temp again
    isHeating = true;
  }
   // variable interval
  delay(INTERVAL);
}

// REMOVED BT COMMAND METHODS HERE
